/*
 * init.sql
 * Copyright (C) 2017 Fabien Kleinbourg <fabien.kleinbourg@cc.in2p3.fr>
 *
 * Distributed under terms of the MIT license.
 */

PRAGMA foreign_keys = on;

CREATE TABLE participants(
    id_participant INTEGER PRIMARY KEY AUTOINCREMENT,
    nom_participant TEXT(30)
);
CREATE TABLE natures(
    id_nature INTEGER PRIMARY KEY AUTOINCREMENT,
    lib_nature TEXT(30)
);
CREATE TABLE familles(
    id_famille INTEGER PRIMARY KEY AUTOINCREMENT,
    lib_famille TEXT(30)
);
CREATE TABLE genres(
    id_genre INTEGER PRIMARY KEY AUTOINCREMENT,
    lib_genre TEXT(30)
);
CREATE TABLE editeurs(
    id_editeur INTEGER PRIMARY KEY AUTOINCREMENT,
    lib_editeur TEXT(30)
);
CREATE TABLE editions(
    id_edi INTEGER PRIMARY KEY AUTOINCREMENT,
    num_edi INTEGER,
    annee_edi INTEGER,
    lib_collection TEXT(30),
    id_editeur NOT NULL,
    FOREIGN KEY (id_editeur) REFERENCES editeurs(id_editeur)
);
CREATE TABLE cds(
    id_cd INTEGER PRIMARY KEY AUTOINCREMENT,
    nom_cd TEXT(30),
    id_famille NOT NULL,
    id_edi NOT NULL,
    FOREIGN KEY (id_famille) REFERENCES familles(id_famille),
    FOREIGN KEY (id_edi) REFERENCES editions(id_edi)
);
CREATE TABLE plages(
    id_plage INTEGER PRIMARY KEY AUTOINCREMENT,
    nom_plage TEXT(30),
    num_plage INTEGER,
    temps_plage INTEGER,
    id_genre NOT NULL,
    id_cd NOT NULL,
    FOREIGN KEY (id_genre) REFERENCES genres(id_genre),
    FOREIGN KEY (id_cd) REFERENCES cds(id_cd),
    CONSTRAINT uq_plages UNIQUE(id_cd, num_plage)
);
CREATE TABLE participations(
    id_participant NOT NULL,
    id_nature NOT NULL,
    id_plage NOT NULL,
    FOREIGN KEY (id_participant) REFERENCES participants(id_participant),
    FOREIGN KEY (id_nature) REFERENCES natures(id_nature),
    FOREIGN KEY (id_plage) REFERENCES plages(id_plage),
    PRIMARY KEY (id_participant, id_nature, id_plage)
);
CREATE TABLE users(
    id_user INTEGER PRIMARY KEY AUTOINCREMENT,
    username TEXT(30),
    password TEXT(60)
);
-- vim:et
