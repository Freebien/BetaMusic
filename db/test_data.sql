/*
 * init.sql
 * Copyright (C) 2017 Fabien Kleinbourg <fabien.kleinbourg@cc.in2p3.fr>
 *
 * Distributed under terms of the MIT license.
 */

INSERT INTO participants(nom_participant) VALUES
    ("Ed Sheeran"),
    ("Geo Gabriel"),
    ("Travis Cole"),
    ("Wayne Hernandez"),
    ("Chris Laws"),
    ("Dann Pursey"),
    ("Joe Rubel"),
    ("Geoff Swan"),
    ("Mark <Spike> Stent"),
    ("Stuart Hawkes"),
    ("Johnny McDaid"),
    ("Steve Mac");
INSERT INTO natures(lib_nature) VALUES
    ("Arrangeur"),
    ("Musicien - Guitare"),
    ("Musicien - Basse"),
    ("Musicien - Batterie"),
    ("Musicien - Piano"),
    ("Musicien - Ukulele"),
    ("Musicien - Violon"),
    ("Musicien - Percussions"),
    ("Compositeur"),
    ("Auteur"),
    ("Producteur"),
    ("Chanteur");
INSERT INTO familles(lib_famille) VALUES
    ("Pop");
INSERT INTO genres(lib_genre) VALUES
    ("Pop");
INSERT INTO editeurs(lib_editeur) VALUES
    ("Asylium Records");
INSERT INTO cds(nom_cd, id_famille, id_edi) VALUES
    ("Divide", 1, 1);
INSERT INTO plages(nom_plage, num_plage, id_genre, id_cd, temps_plage) VALUES
    ("Eraser", 1, 1, 1, 227),
    ("Castle on the Hill", 2, 1, 1, 261),
    ("Dive", 3, 1, 1, 238),
    ("Shape of You", 4, 1, 1, 233),
    ("Perfect", 5, 1, 1, 263),
    ("Galway Girl", 6, 1, 1, 170),
    ("Happier", 7, 1, 1, 207),
    ("New Man", 8, 1, 1, 189),
    ("Hearts Don't Break Around Here", 9, 1, 1, 248),
    ("What Do I Know?", 10, 1, 1, 237),
    ("How Would You Feel (Paean)", 11, 1, 1, 280),
    ("Supermarket Flowers", 12, 1, 1, 221);
INSERT INTO editions(num_edi, annee_edi, lib_collection, id_editeur) VALUES
    (1, 2017, "Atlantic", 1);
INSERT INTO participations(id_participant, id_nature, id_plage) VALUES
    (1, 12, 1),
    (1, 10, 1),
    (1, 9, 1),
    (1, 2, 1),
    (1, 8, 1),
    (2, 12, 1),
    (3, 12, 1),
    (4, 12, 1),
    (5, 4, 1),
    (5, 1, 1),
    (6, 1, 1),
    (7, 1, 1),
    (8, 1, 1),
    (9, 1, 1),
    (10, 1, 1),
    (11, 1, 1),
    (12, 10, 1),
    (13, 10, 1),
    (13, 11, 1),
    (14, 10, 1),
    (14, 11, 1),
    (14, 5, 1);

INSERT INTO users(username, password) VALUES("admin", "$2a$10$Fxil9kJujZdgxmg77jtFY.UyhmXjKQn87ks/waSfWGO3JE3LMn3G2");

-- vim:et
