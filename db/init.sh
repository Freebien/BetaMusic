#! /bin/sh
#
# init.sh
# Copyright (C) 2017 Fabien Kleinbourg <fabien.kleinbourg@cc.in2p3.fr>
#
# Distributed under terms of the MIT license.
#

rm db.sqlite
sqlite3 db.sqlite < init.sql
sqlite3 db.sqlite < test_data.sql
