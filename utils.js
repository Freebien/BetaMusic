/*
 * utils.js
 * Copyright (C) 2017 Fabien Kleinbourg <fabien.kleinbourg@cc.in2p3.fr>
 *
 * Distributed under terms of the MIT license.
 */
var ss = require('socket.io-stream'),
    cfg = require(__dirname + '/config'),
    fs = require('fs'),
    database = require(__dirname + '/db');
module.exports.isAdmin = function (socket){
	if(Object.keys(socket.rooms).indexOf('admin') === -1)
		return false;
	return true;
};
module.exports.isConnected = function(socket){
    if(socket.uuid && Object.keys(sessions).indexOf(socket.uuid) !== -1)
        return true;
    return false;
};
module.exports.play = function(socket, id){
    database.getPlageById(id, function(rows){
        function n(n){
                return n > 9 ? "" + n: "0" + n;
        }
        var stream = ss.createStream();
        var filename = cfg.music.path + "/" + rows.nom_cd + "/" + n(rows.num_plage) + " - " + rows.nom_plage + ".mp3";
        var cover = cfg.music.path + "/" + rows.nom_cd + "/cover.jpg";
        console.log(rows);
        ss(socket).emit('audio-stream', stream, {name: filename});
        socket.emit('title', rows.nom_plage);
        if(fs.existsSync(cover)){
            console.log("cover");
            fs.readFile(cover, function(err, buf){
                if(!err && buf)
                    socket.emit('cover', {image: true, buffer: buf.toString('base64')});
            });
        }
        if(fs.existsSync(filename)){
            fs.createReadStream(filename).pipe(stream);
        }else{
            socket.emit('file_not_on_server');
            console.log('File not found!: ' + filename);
        }
    }, function(err){
        socket.emit('plage_not_found');
    });
};
module.exports.guid = function(){
    this.s4 = function() {
        return Math.floor((1 + Math.random()) * 0x10000)
               .toString(16)
               .substring(1);
    }
    return this.s4() + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' +
           this.s4() + '-' + this.s4() + this.s4() + this.s4();
};
