/*
 * wsbm.js
 * Copyright (C) 2017 Fabien Kleinbourg <fabien.kleinbourg@cc.in2p3.fr>
 *
 * Distributed under terms of the MIT license.
 */
/*
 * Check different inputs
 */
$("#text_input").keypress(function(event){
    if(event.which === 13){
        event.preventDefault();
        if(event.ctrlKey) $("#admin_send_msg").click();
        else $("#send_msg").click();
    }
});
$("#send_msg").click(function(){sendMsg();});
$("#chat_username").keypress(function(event){
    if(event.which === 13){
        event.preventDefault();
        $("#modify_username").click();
    }
});
$("#modify_username").click(changeUsername);
$("#password_input").keypress(function(event){
    if(event.which === 13){
        $("#login_btn").click();
    }
});
$("#login_btn").click(login);
$("#disconnect_btn").click(disconnect);
$("#add_cd").click(function(event){
    var obj = {
        action: "insert",
        type: "cd",
        nom_cd: document.getElementById('titre_cd').value,
        id_famille: document.getElementById('familles-cds').value,
        id_edition: document.getElementById('edition_cd').value
    };
    socket.emit("srv_action", obj);
});

/* WebSocket Definition */
$("#user_li").hide();
var socket = io.connect('http://localhost:8081'),
    username = getCookie("username") || "user_" + String(Math.round(Math.random() * 1000000)),
    usernamebox = $("#chat_username"),
    chatbox = $("#chatbox"),
    uuid = getCookie("session_id"),
    player = document.getElementById('player');
setCookie("username", username);
usernamebox.val(username);

socket.on('connect', function(){
    socket.emit('user_connected', {'username': username, 'session': uuid});
    if(uuid){
        socket.emit('client_back', {uuid: uuid});
    }
});
socket.on('disconnect', function(){
    $.bootstrapGrowl("Disconnected From Server", {type: 'danger'});
});
socket.on('srv_message', function(msg){
	var div = $("<div>").addClass("clearfix").appendTo(chatbox);
    var content = $("<div>").addClass("msg-srv pull-left").appendTo(div);
	$("<p>").addClass("clearfix").text(msg.user + ":").appendTo(content);
	$("<pre>").text(msg.content).appendTo(content);
	chatbox.scrollTop(chatbox.prop("scrollHeight"));
});
socket.on('new_message', function(msg){
	var div = $("<div>").addClass("clearfix").appendTo(chatbox);
    var content = $("<div>").addClass("msg-clients pull-left").appendTo(div);
	$("<p>").addClass("clearfix").text(msg.user + ":").appendTo(content);
	$("<pre>").text(msg.content).appendTo(content);
	chatbox.scrollTop(chatbox.prop("scrollHeight"));
});
socket.on('new_user', function(msg){
	var div = $("<p>").addClass("text-center").html("<b><i>" + msg.user + " s'est connecté</i></b>").appendTo(chatbox);
	chatbox.scrollTop(chatbox.prop("scrollHeight"));
});
socket.on('changed_username', function(msg){
	var div = $("<p>").addClass("text-center").html("<b><i>" + msg.old_user + " est maintenant: " + msg.user + "</i></b>").appendTo(chatbox);
	chatbox.scrollTop(chatbox.prop("scrollHeight"));
});
socket.on('login_success', function(msg){
    setCookie("session_id", msg.session_id);
    uuid = msg.session_id;
    $("#login_li").hide();
    $("#user_li").removeClass("hidden");
    $("#username_client").text(msg.username);
    $("#user_li").show();
    $.bootstrapGrowl("You are successfully logged in");
});
socket.on('user_admin', function(msg){
    console.log("user_admin", msg);
    $("#admin_send_msg").show();
    
});
socket.on('cover', function(msg){
    if(msg.buffer){
        $("#cover").attr("src", "data:image/jpeg;base64," + msg.buffer);
    }
});
socket.on('title', function(msg){
    if(msg){
        $("#title").text(msg);
    }
});
socket.on('list_plages', function(msg){
    //if($.fn.dataTable.isDataTable('#plages-table')){
    //    $("#plages-table").destroy();
    //}
    var tbody = $("#plages-table tbody");
    tbody.empty();
    for(var i in msg.content){
        var tr, numero, titre, genre, buttons, edit, supp, open;
        tr = $("<tr>").appendTo(tbody);
        $('<td>').attr("id", "id-" + msg.content[i].id).text(msg.content[i].id).appendTo(tr);
        td = $("<td>").appendTo(tr);
        $('<input>').attr("id", "plage-num-" + msg.content[i].id).addClass("form-control").val(msg.content[i].num).appendTo(td);
        td = $("<td>").appendTo(tr);
        $('<input>').attr("id", "plage-titre-" + msg.content[i].id).addClass("form-control").val(msg.content[i].titre).appendTo(td);
        td = $("<td>").appendTo(tr);
        $('<select>').attr("id", "plage-genre-" + msg.content[i].id).addClass("form-control").html($("#genres-plages").html()).appendTo(td).val(msg.content[i].id_genre); // val(msg.content[i].famille).appendTo(td);
        buttons = $('<td>').appendTo(tr);
        edit = $("<button>").attr("num", msg.content[i].id).addClass("btn btn-default").appendTo(buttons);
        $("<span>").addClass("glyphicon glyphicon-edit").appendTo(edit);
        edit.click(function(){
            var id = parseInt($(this).attr("num"));
            var titre = $("#plage-titre-" + id).val(),
                genre = $("#plage-genre-" + id).val(),
                num = $("#plage-num-" + id).val();
            socket.emit("srv_action", {action: "update", type: "plage", id: id, titre: titre, genre: genre, num: num});
        });
        supp = $("<button>").attr("num", msg.content[i].id).addClass("btn btn-default").appendTo(buttons);
        $("<span>").addClass("glyphicon glyphicon-remove").appendTo(supp);
        supp.click(function(){
            var id = parseInt($(this).attr("num"));
            socket.emit("srv_action", {action: "delete", type: "plage", id: id});
        });
        open = $("<button>").attr("num", msg.content[i].id).addClass("btn btn-default").appendTo(buttons);
        $("<span>").addClass("glyphicon glyphicon-eye-open").appendTo(open);
        open.click(function(){
            var id = parseInt($(this).attr("num"));
            socket.emit("srv_message", {content: "!participations " + id});
            $(".nav-tabs a[href='#participants']").tab("show");
        });
        play = $("<button>").attr("num", msg.content[i].id).addClass("btn btn-default").appendTo(buttons);
        $("<span>").addClass("glyphicon glyphicon-play").appendTo(play);
        play.click(function(){
            var id = parseInt($(this).attr("num"));
            console.log("!play " + id);
            sendMsg("!play " + id);
        });
    }
    $("#plages-table").DataTable();
});
socket.on('list_participations', function(msg){
    var tbody = $("#participants-table tbody");
    tbody.empty();
    for(var i in msg.content){
        var tr, td, nom, nature, buttons, edit, supp, open;
        tr = $("<tr>").appendTo(tbody);
        td = $("<td>").appendTo(tr);
        $('<select>').attr("id", "participant-nom-" + msg.content[i].id).addClass("form-control").html($("#list-participants").html()).val(msg.content[i].id_participant).appendTo(td);
        td = $("<td>").appendTo(tr);
        $('<select>').attr("id", "participant-nature-" + msg.content[i].id).addClass("form-control").html($("#natures-participants").html()).appendTo(td).val(msg.content[i].id_nature); // val(msg.content[i].famille).appendTo(td);
        buttons = $('<td>').appendTo(tr);
        edit = $("<button>").attr("num", msg.content[i].id).addClass("btn btn-default").appendTo(buttons);
        $("<span>").addClass("glyphicon glyphicon-edit").appendTo(edit);
        edit.click(function(){
            var id = parseInt($(this).attr("num"));
            var nom = $("#participant-nom-" + id).val(),
                nature = $("#participant-nature-" + id).val();
            socket.emit("srv_action", {action: "update", type: "participation", id: id, nom: nom, nature: nature});
        });
        supp = $("<button>").attr("num", msg.content[i].id).addClass("btn btn-default").appendTo(buttons);
        $("<span>").addClass("glyphicon glyphicon-remove").appendTo(supp);
        supp.click(function(){
            var id = parseInt($(this).attr("num"));
            socket.emit("srv_action", {action: "delete", type: "participation", id: id});
        });
        open = $("<button>").attr("num", msg.content[i].id).addClass("btn btn-default").appendTo(buttons);
        $("<span>").addClass("glyphicon glyphicon-eye-open").appendTo(open);
        open.click(function(){
            var id = parseInt($(this).attr("num"));
            socket.emit("srv_message", {content: "!participations " + id});
            $(".nav-tabs a[href='#participants']").tab("show");
        });
    }
    $("#plages-table").DataTable();
});
socket.on('action_failed', function(msg){
    var growl = "Action: " + msg.action + " failed";
    $.bootstrapGrowl(growl, {type: "danger"});
});
socket.on('disconnect_success', function(msg){
    setCookie("session_id", msg.session_id, -1);
    $("#login_li").show();
    $("#user_li").hide();
    // $.bootstrapGrowl("Good Bye");
});
socket.on('disconnected_user', function(msg){
	var div = $("<p>").addClass("text-center").html("<b><i>" + msg.user + " est parti</i></b>").appendTo(chatbox);
	chatbox.scrollTop(chatbox.prop("scrollHeight"));
});
socket.on('list_cds', function(msg){
    // if($.fn.dataTable.isDataTable('#cds-table')){
    //     $("#cds-table").destroy();
    // }
    var tbody = $("#cds-table tbody");
    tbody.empty();
    console.log(tbody);
    for(var i in msg.content){
        var tr, td, edit, buttons, edit, supp, open
        tr = $("<tr>").appendTo(tbody);
        $('<td>').attr("id", "id-" + msg.content[i].id).text(msg.content[i].id).appendTo(tr);
        td = $("<td>").appendTo(tr);
        $('<input>').attr("id", "cd-titre-" + msg.content[i].id).addClass("form-control").val(msg.content[i].titre).appendTo(td);
        td = $("<td>").appendTo(tr);
        $('<select>').attr("id", "cd-famille-" + msg.content[i].id).addClass("form-control").html($("#familles-cds").html()).appendTo(td).val(msg.content[i].id_famille); // val(msg.content[i].famille).appendTo(td);
        td = $("<td>").appendTo(tr);
        $('<select>').attr("id", "cd-edition-" + msg.content[i].id).addClass("form-control").html($("#edition_cd").html()).val(msg.content[i].edition).appendTo(td),
            buttons = $('<td>').appendTo(tr);
        edit = $("<button>").attr("num", msg.content[i].id).addClass("btn btn-default").appendTo(buttons);
        $("<span>").addClass("glyphicon glyphicon-edit").appendTo(edit);
        edit.click(function(){
            var id = parseInt($(this).attr("num"));
            var titre = $("#cd-titre-" + id).val(),
                famille = $("#cd-famille-" + id).val(),
                edition = $("#cd-edition-" + id).val();
            socket.emit("srv_action", {action: "update", type: "cd", id: id, titre: titre, famille: famille, edition: edition});
        });
        supp = $("<button>").attr("num", msg.content[i].id).addClass("btn btn-default").appendTo(buttons);
        $("<span>").addClass("glyphicon glyphicon-remove").appendTo(supp);
        supp.click(function(){
            var id = parseInt($(this).attr("num"));
            socket.emit("srv_action", {action: "delete", type: "cd", id: id});
        });
        open = $("<button>").attr("num", msg.content[i].id).addClass("btn btn-default").appendTo(buttons);
        $("<span>").addClass("glyphicon glyphicon-eye-open").appendTo(open);
        open.click(function(){
            var id = parseInt($(this).attr("num"));
            socket.emit("srv_message", {content: "!plages " + id});
            $(".nav-tabs a[href='#plages']").tab("show");
        });
    }
    var table = $("#cds-table").DataTable();
});
socket.on('list_familles', function(msg){
    var familles = $("#familles-cds");
    familles.empty();
    console.log(msg.content);
    for(var i in msg.content){
        $("<option>").attr("value", msg.content[i].id).text(msg.content[i].nom).appendTo(familles);
    }
});
socket.on('list_editions', function(msg){
    var editions = $("#edition_cd");
    editions.empty();
    console.log(msg.content);
    for(var i in msg.content){
        $("<option>").attr("value", msg.content[i].id).text(msg.content[i].num).appendTo(editions);
    }
});
socket.on('list_participants', function(msg){
    var participants = $("#list-participants");
    participants.empty();
    console.log(msg.content);
    for(var i in msg.content){
        $("<option>").attr("value", msg.content[i].id).text(msg.content[i].nom).appendTo(participants);
    }
});
socket.on('list_genres', function(msg){
    var genres = $("#genres-plages");
    genres.empty();
    console.log(msg.content);
    for(var i in msg.content){
        $("<option>").attr("value", msg.content[i].id).text(msg.content[i].nom).appendTo(genres);
    }
});
socket.on('list_natures', function(msg){
    var natures = $("#natures-participants");
    console.log(natures);
    natures.empty();
    console.log(msg.content);
    for(var i in msg.content){
        $("<option>").attr("value", msg.content[i].id).text(msg.content[i].nom).appendTo(natures);
    }
});

/* Player */
ss(socket).on('audio-stream', function(stream, data) {
    var parts = [];
    stream.on('data', function(chunk){
        parts.push(chunk);
    });
    stream.on('end', function () {
        player.src = (window.URL || window.webkitURL).createObjectURL(new Blob(parts));
        player.play();
    });
});

/* Functions */
function sendMsg(msg=null){
    if(!msg) msg = $("#text_input").val();
    console.log(msg);
    $("#text_input").val("");
	var div = $("<div>").addClass("clearfix").appendTo(chatbox);
    var content = $("<div>").addClass("pull-right msg-me").appendTo(div);
	$("<p>").addClass("clearfix").text("You:").appendTo(content);
	$("<pre>").addClass("msg-me").text(msg).appendTo(content);
	chatbox.scrollTop(chatbox.prop("scrollHeight"));
    if(msg[0] === "!")
        socket.emit('srv_message', {content: msg});
    else
        socket.emit('client_message', {content: msg});
}
function changeUsername(msg=null){
	if(!msg) msg = document.getElementById("chat_username").value;
    username = msg;
    setCookie('chat_username', username);
	socket.emit('change_username', {username: msg});
}
function login(){
    var username = document.getElementById("username_input").value,
        password = document.getElementById("password_input").value;
    socket.emit('login', {username: username, password: password});
}
function disconnect(){
    socket.emit('disconnect_client', {uuid: uuid});
}
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    return cvalue;
}
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
