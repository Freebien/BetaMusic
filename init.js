/*
 * BetaMusic
 *
 * Authors:
 *  Freebien
 *  Mendra
 *
 * # git gud
 *
 */
var cfg = require(__dirname + '/config');
var database = require(__dirname + '/db');
var utils = require(__dirname + '/utils');

var express = require('express');
var app = express();
app.use(express.static(__dirname + '/static'));

var http = require('http').Server(app);

http.listen(cfg.http.port, function(){
    console.log('listening on ' + cfg.http.port);
});

var io = require('socket.io').listen(http);
var sessions = {};

io.on('connection', function(socket){
    console.log('New client connected !');
    database.getFamilles(function(rows){
        socket.emit('list_familles', {content: rows});
    });
    database.getGenres(function(rows){
        socket.emit('list_genres', {content: rows});
    });
    database.getEditions(function(rows){
        socket.emit('list_editions', {content: rows});
    });
    database.getParticipants(function(rows){
        socket.emit('list_participants', {content: rows});
    });
    database.getNatures(function(rows){
        socket.emit('list_natures', {content: rows});
    });
    database.getCds(function(rows){
        socket.emit('list_cds', {content: rows});
    });
    socket.on('user_connected', function(msg){
        if(typeof msg.username !== "undefined"){
            socket.username = msg.username;
            socket.emit('srv_message', {content: 'Vous êtes bien connecté !', user: 'BMBot'});
            socket.emit('connected');
            socket.broadcast.emit('new_user', {user: msg.username});
        }
        if(typeof msg.session !== "undefined" && !msg.session){
            socket.emit('disconnect_success', {session_id: msg.session});
        }
    });
	socket.on('change_username', function(msg){
        if(typeof msg.username !== "undefined"){
            socket.broadcast.emit('changed_username', {old_user: socket.username, user: msg.username});
            socket.username = msg.username;
        }
    });
    socket.on('client_back', function(msg){
        if(typeof msg.uuid !== "undefined"){
            if(typeof sessions[msg.uuid] !== "undefined"){
                socket.uuid = msg.uuid;
                socket.emit("login_success", sessions[socket.uuid]);
            }
        }
    });
    socket.on('login', function(msg){
        if(typeof msg.username !== "undefined" && typeof msg.password !== "undefined"){
            database.login(msg.username, msg.password, function(){
                socket.uuid = utils.guid();
                sessions[socket.uuid] = {session_id: socket.uuid, username: msg.username};
                socket.emit("login_success", sessions[socket.uuid]);
                socket.join("admin");
                socket.emit("user_admin");
            }, function(){
                socket.emit("login_failed");
            });
        }
    });
    socket.on('disconnect_client', function(msg){
        if(typeof msg.uuid !== "undefined"){
            if(typeof sessions[msg.uuid] !== "undefined"){
                delete sessions[msg.uuid];
                socket.emit('disconnect_success', {session_id: msg.uuid});
            }else
                socket.emit('disconnect_fail');
        }else
            socket.emit('disconnect_fail');
    });
    socket.on('client_message', function(msg){
        msg.user = socket.username;
        socket.broadcast.emit('new_message', msg);
    });
    socket.on('srv_action', function(msg){
        console.log(msg);
        switch(msg.type){
            case "cd":
                // if(!utils.isAdmin(socket)) return false;
                switch(msg.action){
                    case "delete":
                        if(typeof msg.id === "number"){
                            database.deleteCd(msg.id, function(){
                                socket.emit('action_success', msg);
                            }, function(){
                                console.log("test");
                                socket.emit('action_failed', msg);
                            });
                        }
                        break;
                    case "update":
                        if(typeof msg.id === "number"){
                            database.updateCd(msg.id, msg, function(){
                                socket.emit('action_success', msg);
                            }, function(){
                                socket.emit('action_failed', msg);
                            });
                        }
                        break;
                    case "insert":
                        console.log("insert CD");
                        if(typeof msg.nom_cd === "string"){
                            database.addCd(msg, function(){
                                console.log("true");
                                socket.emit('action_success', msg);
                            }, function(){
                                console.log("fail");
                                socket.emit('action_failed', msg);
                            });
                        }
                        break;
                    default:
                        socket.emit('unknown');
                }
                break;
            case "plage":
                // if(!utils.isAdmin(socket)) return false;
                switch(msg.action){
                    case "delete":
                        if(typeof msg.id === "number"){
                            database.deletePlage(msg.id, function(){
                                socket.emit('action_success', msg);
                            }, function(){
                                socket.emit('action_failed', msg);
                            });
                        }
                        break;
                    case "update":
                        if(typeof msg.id === "number"){
                            database.updatePlage(msg.id, function(){
                                socket.emit('action_success', msg);
                            }, function(){
                                socket.emit('action_failed', msg);
                            });
                        }
                        break;
                    case "insert":
                        if(typeof msg.id === "number"){
                            database.addPlage(msg.id, function(){
                                socket.emit('action_success', msg);
                            }, function(){
                                socket.emit('action_failed', msg);
                            });
                        }
                        break;
                    default:
                        socket.emit('unknown');
                }
            case "participations":
                // if(!utils.isAdmin(socket)) return false;
                switch(msg.action){
                    case "delete":
                        if(typeof msg.id_plage === "number" && typeof msg.id_participant === "number" && msg.id_nature === "number"){
                            database.deleteParticipation(msg, function(){
                                socket.emit('action_success', msg);
                            }, function(){
                                socket.emit('action_failed', msg);
                            });
                        }
                        break;
                    case "update":
                        if(typeof msg.id_plage === "number" && typeof msg.id_participant === "number" && typeof msg.id_participant === "number"){
                            database.updateParticipation(msg.id_plage, msg, function(){
                                socket.emit('action_success', msg);
                            }, function(){
                                socket.emit('action_failed', msg);
                            });
                        }
                        break;
                    case "insert":
                        if(typeof msg.id_participant === "number" && typeof msg.id_nature === "number"){
                            database.addParticipations(msg.id, function(){
                                socket.emit('action_success', msg);
                            }, function(){
                                socket.emit('action_failed', msg);
                            });
                        }
                        break;
                    default:
                        socket.emit('unknown');
                }
        }
    });
    socket.on('srv_message', function(msg){
        if(typeof msg.content !== "undefined" && msg.content.indexOf("!") === 0){
            var args = msg.content.split(' ');
            switch(args[0]){
                case "!help":
                    socket.emit('srv_message', {content: "!help, !list", user: "BMBot"});
                    socket.emit('srv_message', {content: "Go Fun Yourself", user: "BMBot"});
                    break;
                case "!search":
                    if(args.length < 3)
                        socket.emit('srv_message', {content: "Usage: !search [cd|participant|plage] string to search"})
                    else{
                        if(args[1] === "cd"){
                            database.searchCds(args.slice(2).join(" "), function(rows){
                                socket.emit('srv_message', {content: JSON.stringify(rows, null, ' '), user: "BMBot"});
                            });
                        }
                    }
                    break;
                case "!list":
                    if(args.length !== 1)
                        socket.emit('srv_message', {content: "Usage: !list", user: "BMBot"});
                    else{
                        database.getCds(function(rows){
                            socket.emit('list_cds', {content: rows});
                        });
                    }
                    break;
                case "!familles":
                    database.getFamilles(function(rows){
                        socket.emit('list_familles', {content: rows});
                    });
                    break;
                case "!genres":
                    database.getGenres(function(rows){
                        socket.emit('list_genres', {content: rows});
                    });
                    break;
                case "!editions":
                    database.getEditions(function(rows){
                        socket.emit('list_editions', {content: rows});
                    });
                    break;
                case "!plages":
                    if(args.length == 2){
                        var id = parseInt(args[1]);
                        database.getPlagesFromCd(id, function(rows){
                            socket.emit('list_plages', {id_cd: id, content: rows});
                        });
                    }
                    break;
                case "!play":
                    if(args.length == 2){
                        var id = parseInt(args[1]);
                        utils.play(socket, id);
                    }
                    break;
                case "!participants":
                    if(args.length < 2)
                        database.getParticipants(function(rows){
                            console.log(rows);
                            socket.emit('list_participants', {content: rows});
                        });
                    break;
                case "!natures":
                    if(args.length == 1)
                        database.getNatures(function(rows){
                            console.log(rows);
                            socket.emit('list_natures', {content: rows});
                        });
                    break;
                case "!participations":
                    if(args.length < 2)
                        socket.emit('srv_message', {content: "!participants <id_plage> [num_plage]"});
                    else if(args.length === 2){
                        var id_plage = parseInt(args[1]);
                        database.getParticipationsFromPlage(id_plage, function(rows){
                            console.log(rows);
                            socket.emit('list_participations', {id_plage: id, content: rows});
                        });
                    }
                    break;
                case "!add":
                    if(!utils.isAdmin(socket)){
                        socket.emit('srv_message', {content: "You need admin privileges to do that"});
                        return false
                    }
                    if(args.length >= 3){
                        if(args[1] === "famille"){
                            var arg = args.slice(2).join(' ');
                            database.addFamille(arg, function(){
                                socket.emit("insert_success");
                                database.getFamilles(function(rows){
                                    socket.emit('list_familles', {content: rows});
                                    socket.broadcast.emit('list_familles', {content: rows});
                                });
                            }, function(){
                                socket.emit("insert_failed");
                            });
                        }else if(args[1] === "genre"){
                            database.addGenre(arg, function(){
                                socket.emit("insert_success");
                                database.getGenres(function(rows){
                                        socket.emit('list_genres', {content: rows});
                                        socket.broadcast.emit('list_genres', {content: rows});
                                });
                            }, function(){
                                socket.emit("insert_failed");
                            });
                        }else if(args[1] === "nature"){
                            database.addNature(arg, function(){
                                    socket.emit("insert_success");
                                    database.getNatures(function(rows){
                                        socket.emit('list_natures', {content: rows});
                                        socket.broadcast.emit('list_natures', {content: rows});
                                    });
                                }, function(){
                                    socket.emit("insert_failed");
                                }
                            );
                        }else if(args[1] === "participant"){
                            database.addParticipant(arg, function(){
                                    socket.emit("insert_success");
                                    database.getParticipants(function(rows){
                                        socket.emit('list_participants', {content: rows});
                                        socket.broadcast.emit('list_participants', {content: rows});
                                    });
                                }, function(){
                                    socket.emit("insert_failed");
                                }
                            );
                        }
                    }else
                        socket.emit("srv_message", "usage: !add [famille|genre] <name>");
                    break;
                case "!update":
                    if(!utils.isAdmin(socket)){
                        socket.emit('srv_message', {content: "You need admin privileges to do that"});
                        return false
                    }
                    if(args.length >= 3){
                        var new_args = args.slice(2).join(' ').split('; ');
                        var id = new_args[0];
                        var arg = new_args[1];
                        switch(args[1]){
                            case "famille":
                                database.updateFamille(id, arg, function(){
                                    socket.emit("update_success");
                                    database.getFamilles(function(rows){
                                        socket.emit('list_familles', {content: rows});
                                        socket.broadcast.emit('list_familles', {content: rows});
                                    });
                                }, function(){
                                    socket.emit("update_failed");
                                });
                                break;
                            case "genre":
                                database.updateGenre(id, arg, function(){
                                    socket.emit("update_success");
                                    database.getGenres(function(rows){
                                        socket.emit('list_genres', {content: rows});
                                        socket.broadcast.emit('list_genres', {content: rows});
                                    });
                                }, function(){
                                    socket.emit("update_failed");
                                });
                                break;
                            case "nature":
                                database.updateNature(id, arg, function(){
                                    socket.emit("update_success");
                                    database.getNatures(function(rows){
                                        socket.emit('list_natures', {content: rows});
                                        socket.broadcast.emit('list_natures', {content: rows});
                                    });
                                }, function(){
                                    socket.emit("update_failed");
                                });
                                break;
                            case "participant":
                                database.updateParticipant(id, arg, function(){
                                    socket.emit("update_success");
                                    database.getParticipants(function(rows){
                                        socket.emit('list_participants', {content: rows});
                                        socket.broadcast.emit('list_participants', {content: rows});
                                    });
                                }, function(){
                                    socket.emit("update_failed");
                                });
                                break;
                            default:
                                socket.emit("srv_message", "usage: update [famille|genre|nature|participant] old; new");
                        }
                    }else
                        socket.emit("srv_message", "usage: update [famille|genre|nature|participant] old; new");
                    break;
                case "!delete":
                    if(!utils.isAdmin(socket)){
                        socket.emit('srv_message', {content: "You need admin privileges to do that"});
                        return false
                    }
                    if(args.length === 3){
                        var arg = args.slice(2).join(' ');
                        switch(args[1]){
                            case "famille":
                                database.deleteFamille(arg, function(){
                                    socket.emit("delete_success");
                                    database.getFamilles(function(rows){
                                        socket.emit('list_familles', {content: rows});
                                        socket.broadcast.emit('list_familles', {content: rows});
                                    });
                                }, function(){
                                    socket.emit("delete_failed");
                                });
                                break;
                            case "genre":
                                deleteGenre(arg, function(){
                                    socket.emit("delete_success");
                                    database.getGenres(function(rows){
                                        socket.emit('list_genres', {content: rows});
                                        socket.broadcast.emit('list_genres', {content: rows});
                                    });
                                }, function(){
                                    socket.emit("delete_failed");
                                });
                                break;
                            case "nature":
                                database.deleteNature(arg, function(){
                                    socket.emit("delete_success");
                                    database.getNatures(function(rows){
                                        socket.emit('list_natures', {content: rows});
                                        socket.broadcast.emit('list_natures', {content: rows});
                                    });
                                }, function(){
                                    socket.emit("delete_failed");
                                });
                                break;
                            case "participant":
                                deleteParticipant(arg, function(){
                                    socket.emit("delete_success");
                                    getParticipants(function(rows){
                                        socket.emit('list_participants', {content: rows});
                                        socket.broadcast.emit('list_participants', {content: rows});
                                    });
                                }, function(){
                                    socket.emit("delete_failed");
                                });
                                break;
                            default:
                                socket.emit("srv_message", "usage: !delete [famille|genre|nature|participant] <name>");
                        }
                    }else
                        socket.emit("srv_message", "usage: !delete [famille|genre|nature|participant] <name>");
                    break;
                default:
                    socket.emit("srv_message", "Unkwown command");
            }
        }
    });
    socket.on('play', function(msg){
        if(msg && typeof msg.id !== "number"){
            socket.emit('play_error');
            return false;
        }
        play(socket, msg.id);
    });
    socket.on('disconnect', function(){
        console.log(socket.username + " disconnected");
        socket.broadcast.emit('disconnected_user', {user: socket.username});
    });
});

// db.close();
