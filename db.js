/*
 * BetaMusic
 *
 * Authors:
 *  Freebien
 *  Mendra
 *
 * # git gud
 *
 */
const saltRounds = 10;
const PREQUERY = `PRAGMA foreign_keys=on; `;

var bcrypt = require('bcrypt');
var sqlite3 = require('sqlite3').verbose();
var cfg = require(__dirname + '/config.json');
var db = new sqlite3.Database(__dirname + '/db/db.sqlite');

module.exports.db = db;

module.exports.addCd = function(msg, callback_success=null, callback_failed=null){
    db.serialize(function(){
        console.log("test");
        var obj = {};
        var query = `INSERT INTO cds(nom_cd, id_famille, id_edi) VALUES($nom_cd, $id_famille, $id_edi);`;
        if(typeof msg.id_famille === "number")
            obj.$id_famille = msg.id_famille;
        else
            obj.$id_famille = 1;
        if(typeof msg.id_edition !== "undefined")
            obj.$id_edi = msg.id_edition;
        else
            obj.$id_edi = 1;
        if(typeof msg.nom_cd === "string")
            obj.$nom_cd = msg.nom_cd;
        else
            obj.$nom_cd = "";
        console.log(query);
        db.run(PREQUERY, function(){
            var stmt = db.prepare(query);
            stmt.run(obj, function(err){
                if(!err && typeof callback_success === "function") callback_success();
                else if(err && typeof callback_failed === "function") callback_failed();
            });
            stmt.finalize();
        });
    });
};
module.exports.addPlage = function(id, msg, callback_success=null, callback_failed=null){
    db.serialize(function(){
        var obj = {$id_cd: id};
        var query = `INSERT INTO plages(nom_plage, numèplage, temps_plage, id_genre, id_cd) VALUES($nom_plage, $num_plage, $temps_plage, $id_genre, $id_cd)`;
        if(typeof msg.id_genre === "number")
            obj.$id_genre = msg.id_genre;
        else
            obj.$id_genre = 1;
        if(typeof msg.id_cd === "number")
            obj.$id_cd = msg.id_cd;
        else
            obj.$id_cd = 1;
        if(typeof msg.num_plage === "number")
            obj.$num_plage = msg.num_plage;
        else
            obj.$num_plage = 1;
        if(typeof msg.nom_plage === "string")
            obj.$nom_plage = msg.nom_plage;
        else
            obj.$nom_plage = "";
        if(typeof msg.temps_plage === "string")
            obj.$temps_plage = msg.temps_plage;
        else
            obj.$temps_plage = "";
        var stmt = db.prepare(query);
        db.run(PREQUERY, function(){
            stmt.run(obj, function(err){
                if(!err && typeof callback_success === "function") callback_success();
                else if(err && typeof callback_failed === "function") callback_failed();
            });
            stmt.finalize();
        });
    });
};
module.exports.addFamille = function(nom, callback_success=null, callback_failed=null){
    db.serialize(function(){
        var query = `INSERT INTO familles(lib_famille)
        VALUES(?)`;
        var stmt = db.prepare(query);
        stmt.run(nom, function(err){
            if(!err && typeof callback_success === "function") callback_success();
            else if(err && typeof callback_failed === "function") callback_failed();
        });
        stmt.finalize();
    });
};
module.exports.addGenre = function(nom, callback_success=null, callback_failed=null){
    db.serialize(function(){
        var query = `INSERT INTO genres(lib_genre)
        VALUES(?)`;
        var stmt = db.prepare(query);
        stmt.run(nom, function(err){
            if(!err && typeof callback_success === "function") callback_success();
            else if(err && typeof callback_failed === "function") callback_failed();
        });
        stmt.finalize();
    });
};
module.exports.addNature = function(nom, callback_success=null, callback_failed=null){
    db.serialize(function(){
        var query = `INSERT INTO natures(lib_nature)
        VALUES(?)`;
        var stmt = db.prepare(query);
        stmt.run(nom, function(err){
            if(!err && typeof callback_success === "function") callback_success();
            else if(err && typeof callback_failed === "function") callback_failed();
        });
        stmt.finalize();
    });
};
module.exports.addParticipant = function(nom, callback_success=null, callback_failed=null){
    db.serialize(function(){
        var query = `INSERT INTO participants(nom_participant)
        VALUES(?)`;
        var stmt = db.prepare(query);
        stmt.run(nom, function(err){
            if(!err && typeof callback_success === "function") callback_success();
            else if(err && typeof callback_failed === "function") callback_failed();
        });
        stmt.finalize();
    });
};

module.exports.deleteCd = function(id, callback_success=null, callback_failed=null){
    db.serialize(function(){
        var query = `DELETE FROM cds WHERE id_cd = ?`;
        var stmt = db.prepare(query);
        stmt.run(id, function(err){
            if(!err && typeof callback_success === "function") callback_success();
            else if(err && typeof callback_failed === "function") callback_failed();
        });
        stmt.finalize();
    });
};
module.exports.deleteFamille = function(lib, callback_success=null, callback_failed=null){
    db.serialize(function(){
        var query = `DELETE FROM familles WHERE lib_famille = ?`;
        var stmt = db.prepare(query);
        stmt.run(lib, function(err){
            if(!err && typeof callback_success === "function") callback_success();
            else if(err && typeof callback_failed === "function") callback_failed();
        });
        stmt.finalize();
    });
};
module.exports.deleteGenre = function(lib, callback_success=null, callback_failed=null){
    db.serialize(function(){
        var query = `DELETE FROM genres WHERE lib_genre = ?`;
        var stmt = db.prepare(query);
        stmt.run(lib, function(err){
            if(!err && typeof callback_success === "function") callback_success();
            else if(err && typeof callback_failed === "function") callback_failed();
        });
        stmt.finalize();
    });
};
module.exports.deleteNature = function(lib, callback_success=null, callback_failed=null){
    db.serialize(function(){
        var query = `DELETE FROM natures WHERE lib_nature = ?`;
        var stmt = db.prepare(query);
        stmt.run(lib, function(err){
            if(!err && typeof callback_success === "function") callback_success();
            else if(err && typeof callback_failed === "function") callback_failed();
        });
        stmt.finalize();
    });
};
module.exports.deleteParticipant = function(lib, callback_success=null, callback_failed=null){
    db.serialize(function(){
        var query = `DELETE FROM participants WHERE nom_participant = ?`;
        var stmt = db.prepare(query);
        stmt.run(lib, function(err){
            if(!err && typeof callback_success === "function") callback_success();
            else if(err && typeof callback_failed === "function") callback_failed();
        });
        stmt.finalize();
    });
};
module.exports.deleteParticipation = function(msg, callback_success=null, callback_failed=null){
    db.serialize(function(){
        var query = `DELETE FROM participations WHERE id_participant = ? AND id_nature = ? AND id_plage = ?`;
        var stmt = db.prepare(query);
        stmt.run(msg.id_participant, msg.id_nature, msg.id_plage, function(err){
            if(!err && typeof callback_success === "function") callback_success();
            else if(err && typeof callback_failed === "function") callback_failed();
        });
        stmt.finalize();
    });
};

module.exports.updateCd = function(id, msg, callback_success=null, callback_failed=null){
    db.serialize(function(){
        var obj = {$id_cd: id, $id_famille: 1, $id_edi: 1, $nom_cd: ""};
        var query = `UPDATE cds SET `;
        if(typeof msg.famille !== "undefined"){
            obj.$id_famille = parseInt(msg.famille);
            if(query.indexOf("=") !== -1) query = query + ", ";
            query = query + "id_famille = $id_famille";
        }
        if(typeof msg.edition !== "undefined"){
            obj.$id_edi = parseInt(msg.edition); 
            if(query.indexOf("=") !== -1) query = query + ", ";
            query = query + "id_edi = $id_edi";
        }
        if(typeof msg.titre !== "undefined"){
            obj.$nom_cd = msg.titre;
            if(query.indexOf("=") !== -1) query = query + ", ";
            query = query + "nom_cd = $nom_cd";
        }
        query = query + ` WHERE id_cd = $id_cd`;
        db.run(PREQUERY, function(){
            var stmt = db.prepare(query);
            stmt.run(obj, function(err){
                if(!err && typeof callback_success === "function") callback_success();
                else if(err && typeof callback_failed === "function") callback_failed();
            });
            stmt.finalize();
        });
    });
};
module.exports.updateFamille = function(id, nom, callback_success=null, callback_failed=null){
    db.serialize(function(){
        var query = `UPDATE familles SET lib_famille = ?
        WHERE lib_famille = ?`;
        var stmt = db.prepare(query);
        stmt.run(nom, id, function(err){
            console.log(err);
            if(!err && typeof callback_success === "function") callback_success();
            else if(err && typeof callback_failed === "function") callback_failed();
        });
        stmt.finalize();
    });
};
module.exports.updateGenre = function(id, nom, callback_success=null, callback_failed=null){
    db.serialize(function(){
        var query = `UPDATE genres SET lib_genre = ?
        WHERE lib_genre = ?`;
        var stmt = db.prepare(query);
        stmt.run(nom, id, function(err){
            if(!err && typeof callback_success === "function") callback_success();
            else if(err && typeof callback_failed === "function") callback_failed();
        });
        stmt.finalize();
    });
};
module.exports.updateNature = function(id, nom, callback_success=null, callback_failed=null){
    db.serialize(function(){
        var query = `UPDATE natures SET lib_nature = ?
        WHERE lib_nature = ?`;
        var stmt = db.prepare(query);
        stmt.run(nom, id, function(err){
            if(!err && typeof callback_success === "function") callback_success();
            else if(err && typeof callback_failed === "function") callback_failed();
        });
        stmt.finalize();
    });
};
module.exports.updateParticipant = function(id, nom, callback_success=null, callback_failed=null){
    db.serialize(function(){
        var query = `UPDATE participants SET nom_participant = ?
        WHERE nom_participant = ?`;
        var stmt = db.prepare(query);
        stmt.run(nom, id, function(err){
            if(!err && typeof callback_success === "function") callback_success();
            else if(err && typeof callback_failed === "function") callback_failed();
        });
        stmt.finalize();
    });
};

module.exports.searchCds = function(search, callback=null){
    db.serialize(function(){
        var query = `SELECT id_cd as id, nom_cd as nom
        FROM cds
        WHERE nom_cd LIKE ?
        LIMIT 10`;
        var stmt = db.prepare(query);
        stmt.bind("%" + search + "%");
        stmt.all(function(err, rows){
            if(typeof callback === "function")
                callback(rows);
        });
        stmt.finalize();
    });
};
module.exports.getGenres = function(callback=null){
    db.serialize(function(){
        var query = `SELECT id_genre as id, lib_genre as nom FROM genres`;
        var stmt = db.prepare(query);
        stmt.all(function(err, rows){
            if(typeof callback === "function")
                callback(rows);
        });
        stmt.finalize();
    });
};
module.exports.getFamilles = function(callback=null){
    db.serialize(function(){
        var query = `SELECT id_famille as id, lib_famille as nom FROM familles`;
        var stmt = db.prepare(query);
        stmt.all(function(err, rows){
            if(typeof callback === "function")
                callback(rows);
        });
        stmt.finalize();
    });
};
module.exports.getEditions = function(callback=null){
    db.serialize(function(){
        var query = `SELECT id_edi as id, num_edi as num, annee_edi as annee, lib_collection as collection, id_editeur as editeur FROM editions`;
        var stmt = db.prepare(query);
        stmt.all(function(err, rows){
            if(typeof callback === "function") callback(rows);
        });
    });
};
module.exports.getPlageById = function(id, callback_success=null, callback_fail=null){
    db.serialize(function(){
        var query = `SELECT plages.*, cds.nom_cd
        FROM plages, cds
        WHERE plages.id_cd = cds.id_cd
        AND id_plage = ?`;
        var stmt = db.prepare(query);
        stmt.get(id, function(err, rows){
            if(err && typeof callback_fail === "function")
                callback_fail(rows);
            if(!err && typeof callback_success === "function")
                callback_success(rows);
        });
        stmt.finalize();
    });
};
module.exports.getParticipantsFromCd = function(id_cd, callback=null){
    db.serialize(function(){
        var query = `SELECT DISTINCT(participants.nom_participant) as nom
            FROM participants, participations, plages
            WHERE participations.id_participant = participants.id_participant
            AND participations.id_plage = plages.id_plage
            AND plages.id_cd = ?`;
        var stmt = db.prepare(query);
        stmt.bind(id_cd);
        stmt.all(function(err, rows){
            if(typeof callback === "function")
                callback(rows);
        });
        stmt.finalize();
    });
};
module.exports.getParticipantsFromPlageConcat = function(id_cd, num_plage, callback=null){
    db.serialize(function(){
        var query = `SELECT DISTINCT(participants.nom_participant) as nom, GROUP_CONCAT(natures.lib_nature) as nature
            FROM participants, participations, plages, natures
            WHERE participations.id_participant = participants.id_participant
            AND participations.id_plage = plages.id_plage
            AND participations.id_nature = natures.id_nature
            AND plages.id_cd = ?
            AND plages.num_plage = ?
            GROUP BY participants.nom_participant`;
        var stmt = db.prepare(query);
        stmt.bind(id_cd, num_plage);
        stmt.all(function(err, rows){
            if(typeof callback === "function")
                callback(rows);
        });
        stmt.finalize();
    });
};
module.exports.getParticipants = function(callback=null){
    db.serialize(function(){
        var query = `SELECT id_participant as id, nom_participant as nom FROM participants`;
        var stmt = db.prepare(query);
        stmt.all(function(err, rows){
            if(typeof callback === "function") callback(rows);
        });
        stmt.finalize();
    });
};
module.exports.getNatures = function(callback=null){
    db.serialize(function(){
        var query = `SELECT id_nature as id, lib_nature as nom FROM natures`;
        var stmt = db.prepare(query);
        stmt.all(function(err, rows){
            if(typeof callback === "function") callback(rows);
        });
        stmt.finalize();
    });
};
module.exports.getParticipationsFromPlage = function(id, callback=null){
    db.serialize(function(){
        var query = `SELECT id_participant, id_nature
            FROM participations
            WHERE participations.id_plage = ?`;
        var stmt = db.prepare(query);
        stmt.all(id, function(err, rows){
            if(typeof callback === "function")
                callback(rows);
        });
        stmt.finalize();
    });
};
module.exports.getPlagesFromCd = function(id_cd, callback=null){
    db.serialize(function(){
        var query = `SELECT nom_plage as titre, num_plage as num, id_plage as id, lib_genre as genre, plages.id_genre
            FROM plages, genres
            WHERE plages.id_genre = genres.id_genre
            AND plages.id_cd = ?`;
        var stmt = db.prepare(query);
        stmt.bind(id_cd);
        stmt.all(function(err, rows){
            if(typeof callback === "function")
                callback(rows)
        });
        stmt.finalize();
    });
};
module.exports.getCds = function(callback=null){
    db.serialize(function(){
        var query = `SELECT id_cd as id, nom_cd as titre, lib_famille as famille, cds.id_famille, num_edi as edition
        FROM cds, familles, editions
        WHERE cds.id_famille = familles.id_famille
        AND cds.id_edi = editions.id_edi`;
        db.all(query, function(err, row){
            if(typeof callback === "function")
                callback(row);
        });
    });
};
module.exports.login = function(username, password, callback_success=null, callback_error=null){
    db.serialize(function(){
        var query = `SELECT password FROM users WHERE username = ?`;
        var stmt = db.prepare(query);
        stmt.bind(username);
        stmt.get(function(err, row){
            if(bcrypt.compareSync(password, row.password)){
                if(typeof callback_success === "function")
                    callback_success();
            }else if(typeof callback_error === "function")
                callback_error();
        });
    });
};
module.exports.register = function(username, password, verify_password, callback_success=null, callback_error=null){
    db.serialize(function(){
        if(password !== verify_password){
            callback_error({msg: "Password Mismatch"});
            return false;
        }
        var query = `SELECT COUNT(username) as nb FROM users WHERE username = ?`;
        var stmt = db.prepare(query);
        stmt.bind(username);
        stmt.get(function(err, row){
            if(row.nb !== 0){
                if(typeof callback_error === "function")
                    callback_error({msg: "User already exists, please log in"});
            }else{
                bcrypt.hash(password, saltRounds, function(err, hash){
                    var query = `INSERT INTO users(username, password) VALUES(?, ?)`;
                    var stmt = db.prepare(query);
                    stmt.run(username, hash);
                    stmt.finalize();
                    if(typeof callback_success == "function")
                        callback_success({msg: "Successfully registered. Please Log In"});
                });
            }
        });
    });
};
